var imagenesSecun= []

Vue.component('recientes', {
    template: `
    <div>
    <div class="sidebar-project-wrap mt-30" v-for="publicacion in publicaciones.slice(0,4)">
        <!-- /////////////////////////////////////////////////////////// -->
        <div class="single-sidebar-blog">
            <div class="sidebar-blog-img">
            <a v-for="imagenes in publicacion.img.slice(0,1)">
                <img v-bind:src="imagenes.img" alt="">
            </a>
            </div>
            <div class="sidebar-blog-content">
                <span>{{publicacion.titulo.substring(0,20)}}</span>
                <h4>
                    <a @click="vermas(publicacion.id)">{{publicacion.contenido.substring(0,20)+" ..."}}</a>
                </h4>
            </div>
        </div>
    </div>
</div>
    `,
    mounted() {
        axios.get('http://localhost/APICDS/public/api/publicaciones/lista2')
            .then(res => {
                // console.log(res)
                this.publicaciones = res.data.public

                // console.log(this.publicaciones)
            })
    },
    data() {
        return {
            publicaciones: []
        }
    },
    methods: {
        vermas: function (id) {
            // alert(id)
            window.location.href = "publicacion.html?=" + id
        }

    }
})
const app = new Vue({
    el: "#app",
    created: function () {
        this.getUser();
        // this.public();
    },

    data: {

        publicacion: [],
        parrafos: [],
        imagenesSecundarias:imagenesSecun

    },
    methods: {

        getUser: function () {
            var url = document.location.href;
            var id = url.split('=')[1];
            this.id = id
            axios.get('http://localhost/APICDS/public/api/publicaciones/verID', {
                params: {
                    id: this.id,

                }
            })
                .then(response => {
                    this.publicacion = response.data.datos
                    var cadena = response.data.datos.contenido
                    var separador = "\n\n"
                    var x = cadena.split(separador);
                    var parrafos = []

                    for (let i = 0; i < x.length; i++) {
                        var r = x[i]
                        parrafos.push(r)


                    }
                    this.parrafos = parrafos
                    // para imprimir la imagen principal
                  
                    var res = response.data.datos.img
                    
                    // para imprimir las imagenes secundarias
                   
                    for (let i = 1; i < res.length; i++) {
                        imagenesSecun.push(res[i].img)
                        // console.log(res[i].img);       
                        
                    }

                //   console.log(this.imagenesSecundarias);
                  
                    $("#imagenPricipal").attr("src", res[0].img);
                    

                })
                .catch(error => {
                    console.log(error);
                })
        }


    }



});

