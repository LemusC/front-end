Vue.component('login',{
    props: ['user','rol','id'],
    template:`
    <li class="nav-item dropdown ">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{user}}</a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="navbarDropdown">
            <section v-if="rol=='admin' || rol=='estudiante' || rol=='docente'">
                <a class="dropdown-item" href="../controlNotas/estudiantesmodulo.html" >Notas</a>
                <div class="dropdown-divider"></div>
            </section>
            <section v-if="rol=='admin' || rol=='publicador'">
                <a class="dropdown-item" href="../PublicacionesEventos/crudPublicaciones.html" >Publicaciones y eventos</a> 
                <div class="dropdown-divider"></div>
            </section>
            <section v-if="rol=='alumno'">
                <a class="dropdown-item"  @click="tareaAlumno(id)" >Tareas-Alumno</a> 
                <div class="dropdown-divider"></div>
            </section>
            <section v-if="rol=='admin' || rol=='docente'">
            <a class="dropdown-item" href="../GestionTareas/vistasAlumnos/admin.html" >Tareas-Docente</a> 
            <div class="dropdown-divider"></div>
            </section>
            <a class="dropdown-item" @click="cerrar">Cerrar Sesión</a>          
        </div>          
    </li>
    `,
    methods:{
        // metodo para cerrar sesion al hacer clic
        cerrar:function() {
            // se obtiene el token de la sesionstorage
            var acc=sessionStorage.getItem("acceso");
            // se desencripta el token 
            var acceso=atob(acc);
            // se realiza la petición ajax para cerrar la sesion y se envia como parametro el token
            axios.post('http://localhost/APICDS/public/api/auth/logout',{
                    token:acceso,
                })
                // si se cierra sesion se eliminan los datos guardados en la SessionStorage y se redirecciona al index
            .then(response => {
                sessionStorage.removeItem("acceso");
                sessionStorage.removeItem("user");
                        sessionStorage.removeItem("rol");
                window.location="../SitioWeb/index.html";
            })
            // si ocurre algun error se eliminan los datos guardados en la SessionStorage y se redirecciona al login
            .catch(error => {
                sessionStorage.removeItem("acceso");
                sessionStorage.removeItem("user");
                sessionStorage.removeItem("rol");
                window.location="../SitioWeb/login2-0.html"; 
            });
        },
        // funcion para redireccionar para redirrecionar a la vista de alumno para tareas con su id de user
        tareaAlumno:function(id) {
            window.location="../GestionTareas/alumno/vistaAlumno.html?="+id;
        }
    }
});




//arreglo para mostrar las imagenes en los formularios de modificar y agregar
var imagenes = [];
var idpublicaciones = { idpublic: "" };
// arreglo de imagenes que se van a eliminar en la base de datos 
var eliminar = []
//arreglo de imagenes que se van a insertar  en la base de datos 
var insertar = []

const app = new Vue({
    el: "#app",
    created: function () {
        this.cargar();
        this.mostrar()
       

    },

    data: {
        id: "",
        contenido: '',
        titulo: '',
        autor: '',
        // imagenes = al arreglo de imagenes
        imagenes: imagenes,
        //arreglo obtenido al pulsar el boton actualizar
        items: [],
        publicaciones: [],
        eliminar: eliminar,
        img: "",
        insertar: insertar,
        ////////////////
        //paginacion

        loading: false,
        order: 1,
        searchText: null,
        ccn: null,
        currentPage: 0,
        itemsPerPage: 10,
        resultCount: 0,

        //////////////////////////
        role: '',
        user: '',
        identificador: '',
        //token: atob(this.sessionStorage.getItem("acceso")),
        // fk_usuario: atob(this.sessionStorage.getItem("identificador")),
        fk_usuario: 1

    },
    computed: {
        totalPages: function () {
            // console.log(Math.ceil(this.resultCount / this.itemsPerPage) + "totalPages");
            return Math.ceil(this.resultCount / this.itemsPerPage);

        }
    },
    methods: {
        cargar: function () {

            var acc = sessionStorage.getItem("acceso");


            if (acc != "" && acc != null) {
                var acceso = atob(acc);
                axios.post('http://localhost/APICDS/public/api/auth/refresh', {
                    token: acceso,
                })
                    .then(response => {
                        sessionStorage.removeItem("acceso");
                        // se encripta los datos
                        var acceso2 = btoa(response.data.access_token);
                        var user = btoa(response.data.user.name);
                        var rol = btoa(response.data.rol.name);
                        var id = btoa(response.data.user.id);
                        // se guardan los datos encriptados en SessionStorage
                        sessionStorage.setItem("acceso", acceso2);
                        sessionStorage.setItem("user", user);
                        sessionStorage.setItem("rol", rol);
                        sessionStorage.setItem("identificador", id);
                        // se guarda el nombre del usuario en la variable user
                        this.user = response.data.user.name;
                        // se guarda el nombre del rol en la variable rol
                        this.role = response.data.rol.name;
                        // se guarda el id del usuario en la variable id
                        this.identificador = response.data.user.id;
                        // se indica que la sesion es verdadera
                        this.menuAcceso = true;





                    })
                    .catch(error => {
                        // si la sesion da error se eliminan los datos de la sessionStorage
                        sessionStorage.removeItem("acceso");
                        sessionStorage.removeItem("user");
                        sessionStorage.removeItem("rol");
                        sessionStorage.removeItem("identificador");
                        // window.location="login.html";
                        this.menuAcceso = false;
                        //window.location="login2-0.html"; 

                    });


            } else {
                // this.Acceso=`<menu-basico></menu-basico>`;
                this.menuAcceso = false;
            }

        },

        setPage: function (pageNumber) {
            this.currentPage = pageNumber;
            // console.log(pageNumber);
        },
        encodeImageFileAsURL(event) {
            var file = event.target.files[0];
            var reader = new FileReader();
            reader.onloadend = function () {

                var imgs = { img: "" }
                // insertar las imagenes obtenidas por el formulario al json imgs
                imgs.img = reader.result
                // insertar  el json imgs al arreglo imagenes
                imagenes.push(imgs);

            }
            reader.readAsDataURL(file);

        },
        getImage(event) {
            //Asignamos la imagen a  nuestra data
            var file = event.target.files[0];
            var reader = new FileReader();
            reader.onloadend = function () {

                var imgs = { img: "", idpublic: "" }
                // insertar las imagenes obtenidas por el formulario al json imgs
                imgs.img = reader.result
                imgs.idpublic = idpublicaciones.idpublic
                // insertar  el json imgs al arreglo imagenes
                imagenes.push(imgs);

                var imgPu = { img: "", id: "" }
                // insertar las imagenes obtenidas por el formulario al json imgs
                imgPu.img = reader.result
                imgPu.id = idpublicaciones.idpublic
                // insertar  el json imgs al arreglo imagenes
                insertar.push(imgPu);


            }
            reader.readAsDataURL(file);

            // console.log(file);

        },

        mostrar() {

                        // se obtiene el token de la sesionstorage
                        let acc=sessionStorage.getItem("acceso");
                        // se desencripta el token 
                        let acceso=atob(acc);
            axios.get('http://localhost/APICDS/public/api/publicaciones/lista3?token='+acceso)
                .then(response => {
                    //asignarle al arreglo publicaciones la respuesta donde obtenemos todas las publicaciones

                    var ordenar = response.data.public
                    ordenar.sort((a, b) => b.id - a.id);
                    // console.log(ordenar);
                    this.publicaciones = ordenar

                })
                .catch(error => {
                    console.log(error);
                })
        },
        /////////////////////////////////////////////////////////  para eliminar imagenes   /////////////////////////////////////////////////
        borrar: function (img) {
            //for para comparar las images para poder eliminarlas de el arreglo con splice      
            for (var i = 0; i < imagenes.length; i++) {
                if (imagenes[i].img == img) {
                    imagenes.splice(i, 1);
                    break;
                }
            }
        },
        ///////////////////////////////////////// Agregar publicacion  //////////////////////////////////////////////
        limpiar: function () {
            this.contenido = '',
                this.titulo = '',
                this.autor = '',

                $("#agre").removeAttr("data-dismiss", "modal");
            for (let i = 0; i < imagenes.length; i++) {
                imagenes.splice(i);
             
            }
        },
        agregar: function (data) {

            // console.log(this.token);
            // console.log(this.fk_usuario);


            if (this.contenido && this.titulo && this.autor && this.imagenes != null && this.imagenes != "") {
                /*si los campos estan llenos  */
                $("#agre").attr("data-dismiss", "modal");
                            // se obtiene el token de la sesionstorage
                            var acc=sessionStorage.getItem("acceso");
                            // se desencripta el token 
                            var acceso=atob(acc);
                axios.post('http://localhost/APICDS/public/api/publicaciones/insertar', {
                    contenido: this.contenido,
                    titulo: this.titulo,
                    autor: this.autor,
                    fk_usuario: this.fk_usuario,
                    imagenes: this.imagenes,
                    token: this.token
                })

                    .then(response => {

                        // console.log(response);
                        this.mostrar()
                        Swal.fire({

                            type: 'success',
                            title: 'Publicacion Realizada',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        this.contenido = '',
                            this.titulo = '',
                            this.autor = ''


                    })
                    .catch(error => {
                        console.log(error);
                        alert(error)
                    })

            } else {

                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: '¡Rellene todos los campos !',
                })

            }





        },
        /////////////////////////////////////    eliminar Publicacion   ///////////////////////////////////////////////
        vaciarDatosForm: function () {
            this.contenido = '',
                this.titulo = '',
                this.autor = ''

        },
        elimina: function (id) {


            Swal.fire({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, bórralo!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        '¡Eliminado!',
                        'Su Publicacion ha sido eliminado.',
                        'success'
                    )
                                    // se obtiene el token de la sesionstorage
                    var acc=sessionStorage.getItem("acceso");
                    // se desencripta el token 
                    var acceso=atob(acc);
                    axios.get('http://localhost/APICDS/public/api/publicaciones/eliminar', {
                        params: {
                            id: id
                        },
                        
                    })
                        .then(response => {
                            // console.log(response);
                            this.mostrar()
                        })
                        .catch(error => {
                            console.log(error);
                            alert(error)
                        })

                }
            })

        },
        //////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////    Actualizar Publicacion   /////////////////////////////////////////////////////////////
        actualizar: function (item) {

            // asignandole a el arreglo items  el arreglo item que obtenemos al pulsar el boton actualizar
            this.items = item


            for (let i = 0; i < insertar.length; i++) {
                insertar.splice(i);

            }

            for (let i = 0; i < eliminar.length; i++) {
                eliminar.splice(i);

            }

            for (let i = 0; i < imagenes.length; i++) {
                imagenes.splice(i);
                // $('.delete').remove();
            }
            // asignandole a el arreglo h  el valor de el arreglo de imagenes            
            var h = this.items.img
            // for para recorrer el arreglo h que contiene todas las imagenes que obtenemos de la publicacion seleccionada
            for (let i = 0; i < h.length; i++) {
                // obteniendo el valor de una imagen
                var m = h[i].img;
                var d = h[i].id;

                var imgs = { img: "", id: "", idpublic: "" }
                // insertar la imagen obtenida por cada giro y asignarle la imagen a el json imgs
                imgs.img = m
                imgs.id = d
                imgs.idpublic = item.id
                // insertar  el json imgs al arreglo imagenes
                imagenes.push(imgs);

            }
            idpublicaciones.idpublic = item.id

        }
        ,
        /////////////////////////////////////////////////////////  para eliminar imagenes   /////////////////////////////////////////////////
        borrar2: function (imgs) {

            //asignandole el valor de la imagen que obtenemos al pulsar el boton de borrar imagen y selo asignamos a la varible img
            var img = imgs.img
            var id = imgs.id


            var eli = { id: "" }
            eli.id = imgs.id
            eliminar.push(eli)


            this.img = img
            //for para comparar las images para poder eliminarlas de el arreglo con splice 
            for (var i = 0; i < imagenes.length; i++) {
                if (imagenes[i].img == img) {
                    imagenes.splice(i, 1);
                    break;
                }
            }
            //for para comparar las images para poder eliminarlas de el arreglo insertar con splice 
            for (var i = 0; i < insertar.length; i++) {
                if (insertar[i].img == img) {
                    insertar.splice(i, 1);
                    break;
                }
            }


        },
        /////////////////////////////////////////////////////////////////////////

        modificar: function (id) {
                        // se obtiene el token de la sesionstorage
                        var acc=sessionStorage.getItem("acceso");
                        // se desencripta el token 
                        var acceso=atob(acc);
            axios.put('http://localhost/APICDS/public/api/publicaciones/modi', {

                id: id,
                contenido: this.items.contenido,
                titulo: this.items.titulo,
                autor: this.items.autor,
                fk_usuario: this.fk_usuario,
                eliminar: this.eliminar,
                insertar: this.insertar,
                token:this.token
            })

                .then(response => {
                    this.mostrar()
                    Swal.fire({
                        type: 'success',
                        title: 'Modificación con Éxito',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
                .catch(error => {
                    console.log(error);
                    alert(error)
                })

        }
        ,
        /////////////////////////////////////    eliminar Publicacion   ///////////////////////////////////////////////

        vermas: function (id) {

            window.location.href = "publicacion.html?=" + id

        },
    },
    filters: {
        paginate: function (list) {
            this.resultCount = this.publicaciones.length;
            // console.log(this.resultCount + " Result count");
            // console.log(this.currentPage + " current page");
            // console.log(this.itemsPerPage + " items per page");
            // console.log(this.totalPages + " Total pages 2");
            if (this.currentPage >= this.totalPages) {
                this.currentPage = Math.max(0, this.totalPages - 1);
            }
            var index = this.currentPage * this.itemsPerPage;
            // console.log(index + " index");
            // console.log(this. publicaciones.slice(index, index + this.itemsPerPage));
            return this.publicaciones.slice(index, index + this.itemsPerPage);
        }
    }
});