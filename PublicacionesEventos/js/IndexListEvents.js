$("#imgH").hide();
$("#imgM").hide();

const app = new Vue({
    el: "#app",
    created: function () {
        this.mostrar()

    },

    data: {
        id: "",
        contenido: '',
        titulo: '',
        fecha: '',
        contacto: '',
        img: '',
        items: [],
        eventos: [],
        //////////////////////////////
        loading: false,
        order: 1,
        searchText: null,
        ccn: null,
        currentPage: 0,
        itemsPerPage: 10,
        resultCount: 0,
        token: atob(this.sessionStorage.getItem("acceso"))

    },

    computed: {
        totalPages: function () {
            // console.log(Math.ceil(this.resultCount / this.itemsPerPage) + "totalPages");
            return Math.ceil(this.resultCount / this.itemsPerPage);

        },
    },
    methods: {
        setPage: function (pageNumber) {
            this.currentPage = pageNumber;
            // console.log(pageNumber);
        },

        getImage(event) {
            //Asignamos la imagen a  nuestra data
            var file = event.target.files[0];
            var reader = new FileReader();
            reader.onloadend = function () {
                $("#img").val(reader.result);
                $("#img2").val(reader.result);
                $("#imagen").attr("src", reader.result);
                document.getElementById("imagenR").src = reader.result;

                var imagen = reader.result

                // console.log(imagen);

            }
            reader.readAsDataURL(file);
        },


        mostrar: function () {
               // se obtiene el token de la sesionstorage
        var acc=sessionStorage.getItem("acceso");
        // se desencripta el token 
        var acceso=atob(acc);
            axios.get('http://localhost/APICDS/public/api/eventos/lista3?token='+this.token)
                .then(response => {
                    var ordenar = response.data.eventos
                    ordenar.sort((a, b) => b.id - a.id);
                    this.eventos = ordenar


                })
                .catch(error => {
                    console.log(error);
                })
        },
        limpiar: function (data) {

            $("#agre").removeAttr("data-dismiss", "modal");

        },

        ///////////////////////////////////////// Agregar publicacion  //////////////////////////////////////////////

        agregar: function (data) {
            this.img = $("#img").val();
            fechaN = new Date();
            fechaN = moment(fechaN, 'DD/MM/YYYY').format('YYYY-MM-DD');
            if (this.fecha >= fechaN) {

                if (this.contacto && this.contenido && this.titulo && this.fecha != null) {
                              // se obtiene el token de la sesionstorage
                    var acc=sessionStorage.getItem("acceso");
                    // se desencripta el token 
                    var acceso=atob(acc);
                    $("#agre").attr("data-dismiss", "modal");
                    axios.post('http://localhost/APICDS/public/api/eventos/insertar?token='+this.token, {

                        contenido: this.contenido,
                        fecha: this.fecha,
                        contacto: this.contacto,
                        titulo: this.titulo,
                        img: this.img,
                        fk_usuario:1
                        

                    })

                        .then(response => {

                            // console.log(response);
                            this.mostrar()
                            Swal.fire({
                                // position: 'top-end',
                                type: 'success',
                                title: 'Evento Agregado',
                                showConfirmButton: false,
                                timer: 1500
                            })

                        })
                        .catch(error => {
                            console.log(error);
                            // alert(error)
                            Swal.fire({
                                type: 'error',
                                title: 'Evento no agregardo "fecha menor a la actual"',

                                // text: '¡Algo salió mal!',
                                // footer: '<a href>Why do I have this issue?</a>'
                            })
                            // $("#AGGPublic").modal(show)
                            // $('#AGGPublic').modal('show')

                        })
                } else {
                    Swal.fire({
                        type: 'error',
                        title: 'Rellene todos los campos',
                        // text: '¡Algo salió mal!',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                }


            } else {

                Swal.fire({
                    type: 'error',
                    title: '"fecha menor a la actual"',

                    // text: '¡Algo salió mal!',
                    // footer: '<a href>Why do I have this issue?</a>'
                })



            }






        },
        /////////////////////////////////////    eliminar Publicacion   ///////////////////////////////////////////////

        eliminar: function (id) {

            Swal.fire({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, bórralo!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Eliminado',
                        '!Su archivo ha sido eliminado!',
                        'success'
                    )
                                // se obtiene el token de la sesionstorage
                                var acc=sessionStorage.getItem("acceso");
                                // se desencripta el token 
                                var acceso=atob(acc);
                    axios.get('http://localhost/APICDS/public/api/eventos/eliminar?token='+this.token, {
                        params: {
                            id: id
                        },
                       
                       })
                        .then(response => {
                            // console.log(response);
                            this.mostrar()
                        })
                        .catch(error => {
                            console.log(error);
                            alert(error)
                        })
                }
            })

        },
        //////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////    Actualizar Publicacion   /////////////////////////////////////////////////////////////
        actualizar: function (item) {
            this.items = item
        },
        /////////////////////////////////////////////////////////////////////////

        modificar: function (id) {
            this.items.img = $("#img2").val();
            //   console.log(this.items.img);

                        // se obtiene el token de la sesionstorage
                        var acc=sessionStorage.getItem("acceso");
                        // se desencripta el token 
                        var acceso=atob(acc);
            axios.put('http://localhost/APICDS/public/api/eventos/modi?token='+this.token, {

                id: id,
                contenido: this.items.contenido,
                fecha: this.items.fecha,
                contacto: this.items.contacto,
                titulo: this.items.titulo,
                img: this.items.img


            })

                .then(response => {

                    // console.log(response);
                    this.mostrar()
                    Swal.fire({
                        // position: 'top-end',
                        type: 'success',
                        title: 'Evento Actualizado',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
                .catch(error => {
                    console.log(error);
                    alert(error)
                    Swal.fire({
                        type: 'error',
                        title: 'Ups ...',
                        text: '¡Algo salió mal!',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                })

        }

    },
    filters: {
        paginate: function (list) {
            this.resultCount = this.eventos.length;
            // console.log(this.resultCount + " Result count");
            // console.log(this.currentPage + " current page");
            // console.log(this.itemsPerPage + " items per page");
            // console.log(this.totalPages + " Total pages 2");
            if (this.currentPage >= this.totalPages) {
                this.currentPage = Math.max(0, this.totalPages - 1);
            }
            var index = this.currentPage * this.itemsPerPage;
            // console.log(index + " index");
            // console.log(this.eventos.slice(index, index + this.itemsPerPage));
            return this.eventos.slice(index, index + this.itemsPerPage);
        }
    }



});