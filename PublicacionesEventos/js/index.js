var elemento;
var elementoActivo = false;

var movil = false;

var cCarga = 0;
var estaCargando = true;

var colores = {c1 : '#81BEF7', c1 : '#81F79F', c1 : '#CC2EFA', c1 : '#A4A4A4', c1 : '#F78181'};

$(function(){
  
  
  var nav = $('.menuControl'),
      navBut = $('.navBut');
  
  var menu1 = $('.elemento1'), 
      menu1v = $('.adminControl'),
      menu1c = $('.adminCerrar')
      ;
  
  var menu2 = $('.elemento2'), 
      menu2v = $('.buscarControl'),
      menu2c = $('.buscarCerrar')
      ;
  
  var menu3 = $('.elemento3'), 
      menu3v = $('.nuevoControl'),
      menu3c = $('.nuevoCerrar')
      ;
  
  var menu4 = $('.elemento4'), 
      menu4v = $('.eliminarControl'),
      menu4c = $('.eliminarCerrar')
      ;
  
  var menu5 = $('.elemento5'), 
      menu5v = $('.datosControl'),
      menu5c = $('.datosCerrar')
      ;
  
  var menu6 = $('.elemento6'), 
      menu6v = $('.ayudaControl'),
      menu6c = $('.ayudaCerrar')
      ;
  
  
  $("input[id='nuevo2']").keyup(function() {
    $(this).val($(this).val().replace(/^(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/, "($1)$2-$3-$4-$5"));
});

  
  //bloque para iniciar menu
  navBut.click(function(){
    if(nav.height() === 0){
      nav.stop().animate({ height: '100%', opacity: '1.0' }, 400);
      navBut.css('color', '#642EFE');
      //moverMenuInicio();
      
    } else {
      
      if(elementoActivo){
        elemento.hide('fast');
      }
      
      nav.stop().animate({ height: '0', opacity: '0.0' }, 400);
      navBut.css('color', '#585858');
    }
  });
  
  //eventos para mostrar menuses
  menu1.click(function(){
    
    menu(menu1v);
    
      
  });
  
  menu1c.click(function(){
    menuCerrar(menu1v);
    
    if(movil){
      moverMenuInicio();
    }
    
  });
  
  
  //eventos para mostrar menuses
  menu2.click(function(){
    
    menu(menu2v);
    
  });
  
  menu2c.click(function(){
    menuCerrar(menu2v);
    
    if(movil){
      moverMenuInicio();
    }
  });
  
  
  //eventos para mostrar menuses
  menu3.click(function(){
    
    menu(menu3v);
    
  });
  
  menu3c.click(function(){
    menuCerrar(menu3v);
    
    if(movil){
      moverMenuInicio();
    }
  });
  
  
  //eventos para mostrar menuses
  menu4.click(function(){
    
    menu(menu4v);
    
  });
  
  menu4c.click(function(){
    menuCerrar(menu4v);
    
    if(movil){
      moverMenuInicio();
    }
  });
  
  
  //eventos para mostrar menuses
  menu5.click(function(){
    
    menu(menu5v);
    
  });
  
  menu5c.click(function(){
    menuCerrar(menu5v);
    
    if(movil){
      moverMenuInicio();
    }
  });
  
  
  //eventos para mostrar menuses
  menu6.click(function(){
    
    menu(menu6v);
    
  });
  
  menu6c.click(function(){
    menuCerrar(menu6v);
    
    if(movil){
      moverMenuInicio();
    }
  });
  
  
  //verificar tamaño
  checkWidth();
  $(window).resize(checkWidth);
  
  
  
  
})

/*
$(document).ready(function() {
  $('.elemento1').click(function() {
    $("html, body").animate(
      {
        scrollTop: $(".adminControl").offset().top - 50
      },
      1200
    );
  });
});
*/


//funcion para mover en menu
function moverMenu(elemento){
  
  $("html, body").animate(
      {
        scrollTop: elemento.offset().top 
      },
      800
    );
}

function moverMenuInicio(){
  $("html, body").animate(
      {
        scrollTop: 0 
      },
      1200
    );
}



//funciones de menu (abrir-cerrar)
function menu(elementov){
  //eventos para mostrar menuses
    if(elementoActivo){
        elemento.slideUp('medium');
      }
    
    if(elementov.css('display') == 'none'){
      
      elementov.slideDown('medium',function(){
        
        if (movil) moverMenu(elementov);
      });
      
      elementoActivo = true;
      elemento = elementov;
    }else{
      elementov.slideUp('slow');
      elementoActivo = false;
    }
      
}

function menuCerrar(elementov){
  
  elementov.slideUp('slow');
  elementoActivo = false;
  
}


//funcion para checar ancho
function checkWidth() {
  var windowsize = $(window).width();
  if (windowsize < 768) {
    //movil
    movil = true;
    //alert("movil");
  } else{
    //alert("no movil");
    movil = false;
  }
  
}