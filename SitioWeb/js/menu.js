
// se crea una aplicacion de vue con el id app
new Vue({
    el: "#app",
    // se definen los datos
    data:{
        datos:"",
        id0:'',
        id1:'',
        id2:'',
        id3:'',
        id4:'',
        id5:'',
        imagen0:'',
        imagen1:'',
        imagen2:'',
        imagen3:'',
        imagen4:'',
        imagen5:'',
        titulo0:'',
        titulo1:'',
        titulo2:'',
        titulo3:'',
        titulo4:'',
        titulo5:'',
        contenido0:'',
        contenido1:'',
        contenido2:'',
        contenido3:'',
        contenido4:'',
        contenido5:'',
        eventos:[],
        publicaciones: [],
        modalEvento:'',
    },
    // funcion para cargar al iniciar app
    created(){
        this.cargarPublicaciones();
        // this.cargar(); 
        this.verEventos();
        // this.mostrar();
    },
    //se definen los metodos a utilizar
    methods:{
        // funcion para   carrusel de index
        cargarPublicaciones:function() {
            // axios.get('http://192.168.32.90/Publicaciones_eventos2/apiRest/public/api/publicaciones/lista2')
            axios.get('http://localhost/APICDS/public/api/publicaciones/lista2')
            .then(response => {
                // se guardan los datos recibidos de la base de datos en una variable
                var datos= response.data;
                // se evalua si existe alguna imagen en cada if y se guarda en un arreglo en la posision 0
                if (datos.public[0].img.length>0) {
                    // se guarad el valor de la imagen 
                    this.imagen0=datos.public[0].img[0].img;
                    //se guarda el valor del titulo
                    this.titulo0=datos.public[0].titulo;
                    //se guarda el valor del contenido
                    this.contenido0=datos.public[0].contenido;
                    this.id0=datos.public[0].id;
                }
                    // se evalua si existe alguna imagen y se guarda en un arreglo en la posision 1
                if (datos.public[1].img.length>0) {
                    this.imagen1=datos.public[1].img[0].img;
                    this.titulo1=datos.public[1].titulo;
                    this.contenido1=datos.public[1].contenido;
                    this.id1=datos.public[1].id;
                }
                // se evalua si existe alguna imagen y se guarda en un arreglo en la posision 2
                if (datos.public[2].img.length>0) {
                    this.imagen2=datos.public[2].img[0].img;
                    this.titulo2=datos.public[2].titulo;
                    this.contenido2=datos.public[2].contenido;
                    this.id2=datos.public[2].id;
                }
                // se evalua si existe alguna imagen y se guarda en un arreglo en la posision 3
                if (datos.public[3].img.length>0) {
                    this.imagen3=datos.public[3].img[0].img;
                    this.titulo3=datos.public[3].titulo;
                    this.contenido3=datos.public[3].contenido;
                    this.id3=datos.public[3].id;
                }
                // se evalua si existe alguna imagen y se guarda en un arreglo en la posision 4
                if (datos.public[4].img.length>0) {
                    this.imagen4=datos.public[4].img[0].img;
                    this.titulo4=datos.public[4].titulo;
                    this.contenido4=datos.public[4].contenido;
                    this.id4=datos.public[4].id;
                }
                // se evalua si existe alguna imagen y se guarda en un arreglo en la posision 5
                if (datos.public[5].img.length>0) {
                    this.imagen5=datos.public[5].img[0].img;
                    this.titulo5=datos.public[5].titulo;
                    this.contenido5=datos.public[5].contenido;
                    this.id5=datos.public[5].id;
                }
                
                
            })
            .catch(error => {
                console.log(error);
            });
        },

        // funcion para mostrar eventos en index
        verEventos:function () {
            // axios.get('http://192.168.32.90/Publicaciones_eventos2/apiRest/public/api/eventos/lista')
            axios.get('http://localhost/APICDS/public/api/eventos/lista')
                .then(response => {
                // this.eventos = response.data;
                var listaE=response.data.eventos;
                var a=0;
                var longitud=listaE.length-1;
                var cadena;
                var cadena2="";
                for (let i = longitud; i >=0 ; i=i-1) {
                    // console.log(listaE[i]);
                    if (listaE[i].img!=null) {
                        if (listaE[i].contenido.length>1  && a<3) {
                            cadena=listaE[i].contenido.split("\n\n");
                            // s eobtiene la fecha del json
                            var fechaE= new Date(listaE[i].fecha);
                            // console.log(fechaE);
                            // se obtiene el valor del dia simandole 1
                            var dia= fechaE.getDate()+1;
                            // se obtiene el valor del mes simandole 1
                            var mes= fechaE.getMonth()+1;
                            // se obtiene el valor del año
                            var year= fechaE.getFullYear();
                            // se guarda un objeto en cada indice segun el bucle
                            this.eventos.push({
                                titulo:listaE[i].titulo,
                                img: listaE[i].img,
                                contenido:cadena,
                                cuerpo:cadena[0],
                                id: listaE[i].id,
                                fecha: dia+"-"+mes+"-"+year,
                            });
                            a++;
                        }
                    } 

                }
                
                })
                .catch(error => {
                console.log(error)

                })
        },
        // funcion para redireccionar a una publicacion
        vermas: function (id) {
            // alert(id)
            window.location.href = "../PublicacionesEventos/publicacion.html?="+id
        },
        // mostrar modal con informacion completa del index
        verModal:function (evento) {
            // se guardan los datos a mostrar en el modal en una json
            this.modalEvento={
                titulo:evento.titulo,
                cuerpo:evento.contenido,
                img: evento.img,
                fecha: evento.fecha
            };
            // se indica mostrar modal con Jquery
            $("#modalEventos").modal("show");
        },
       
        



    }

});