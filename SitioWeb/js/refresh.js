$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    var altura = $('.sticky-top').offset().top;
	// se utilza el scroll pra que el menu se situe al princioi de la pagina
	$(window).on('scroll', function(){
		if ( $(window).scrollTop() > altura ){
			$('.sticky-top').addClass('menu-fixed');
		} else {
			$('.sticky-top').removeClass('menu-fixed');
		}
    });
    
    var URLactual = window.location.pathname; //utilizada para detemrinar que tab del menu activar
    URLactual = URLactual.split('/').pop();//la url se convierte en arreglo y se obtiene el ultimo elemento 
    URLactual = URLactual.split('.');// se indica que se convierta en arraglo al encontara .
    $('.' + URLactual[0] ).addClass('active2');

});

//  componente para mostrar menu si no esta logueado
Vue.component('menu-basico', {
    // plantilla del menu
    template: `
    <nav class="navbar navbar-expand-lg navbar-light bg-menu sticky-top">
        <!-- imagen de logo  -->
        <a class="navbar-brand" href="../SitioWeb/index.html">
            LOGO
        </a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- links del menu -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item index"> 
                    <a class="nav-link" href="../SitioWeb/index.html">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item quienes_somos">
                    <a class="nav-link" href="../SitioWeb/quienes_somos.html">¿Quiénes somos?</a>
                </li>
                <li class="nav-item main-publicacion">
                    <a class="nav-link" href="../PublicacionesEventos/main-publicacion.html">Publicaciones</a>
                </li>
                <li class="nav-item vistaDeEventos">
                    <a class="nav-link" href="../PublicacionesEventos/vistaDeEventos.html">Eventos</a>
                </li>
                <li class="nav-item contactanos" >
                    <a class="nav-link" href="../SitioWeb/contactanos.html">Contáctanos</a>
                </li>
                <li class="nav-item login2-0 ">
                    <a class="nav-link btn btn-primary" id="b-sesion" href="../SitioWeb/login2-0.html">Iniciar Sesion</a>
                </li>
            </ul>

        </div>
    </nav>

`,
});



// componente para mostarar menu si esta logueado
Vue.component('menu-log', {
    // se reciben parametros para funcionalidad del menu
    props: ['user','rol','id'],
    // plantilla para el menu, el codigo html a utilizar
    template: `
    <nav class="navbar navbar-expand-lg navbar-light bg-menu sticky-top" id="menuA">
        <!-- imagen de logo  -->
        <a class="navbar-brand" href="../SitioWeb/index.html">
           LOGO
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- links del menu -->
            <ul class="navbar-nav ml-auto p-2">
                <li class="nav-item index" >
                    <a class="nav-link " href="../SitioWeb/index.html">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item quienes_somos" >
                    <a class="nav-link" href="../SitioWeb/quienes_somos.html ">¿Quiénes somos?</a>
                </li>
                <li class="nav-item main-publicacion">
                    <a class="nav-link" href="../PublicacionesEventos/main-publicacion.html">Publicaciones</a>
                </li>
                <li class="nav-item vistaDeEventos">
                    <a class="nav-link" href="../PublicacionesEventos/vistaDeEventos.html">Eventos</a>
                </li>
                <li class="nav-item contactanos" >
                    <a class="nav-link" href="../SitioWeb/contactanos.html">Contáctanos</a>
                </li>
                <li class="nav-item dropdown  mr-5">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{user}}</a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="navbarDropdown">
                        <section v-if="rol=='admin' || rol=='estudiante' || rol=='docente'">
                            <a class="dropdown-item" href="../controlNotas/estudiantesmodulo.html" >Notas</a>
                            <div class="dropdown-divider"></div>
                        </section>
                        <section v-if="rol=='admin' || rol=='publicador'">
                            <a class="dropdown-item" href="../PublicacionesEventos/crudPublicaciones.html" >Publicaciones y eventos</a> 
                            <div class="dropdown-divider"></div>
                        </section>
                        <section v-if="rol=='estudiante'">
                            <a class="dropdown-item"  href="../GestionTareas/vistasAlumnos/vistaAlumno.html" >Tareas-Alumno</a> 
                            <div class="dropdown-divider"></div>
                        </section>
                        <section v-if="rol=='admin' || rol=='docente'">
                        <a class="dropdown-item" href="../GestionTareas/vistasAlumnos/admin.html" >Tareas-Docente</a> 
                        <div class="dropdown-divider"></div>
                        </section>
                        <a class="dropdown-item" @click="cerrar">Cerrar Sesión</a>          
                    </div>          
                </li>
            </ul>
        </div>
    </nav>
`,
// metodos relacionados al menu
methods:{
    // metodo para cerrar sesion al hacer clic
    cerrar:function() {
        // se obtiene el token de la sesionstorage
        var acc=sessionStorage.getItem("acceso");
        // se desencripta el token 
        var acceso=atob(acc);
        // se realiza la petición ajax para cerrar la sesion y se envia como parametro el token
        axios.post('http://localhost/APICDS/public/api/auth/logout',{
                token:acceso,
            })
            // si se cierra sesion se eliminan los datos guardados en la SessionStorage y se redirecciona al index
        .then(response => {
            sessionStorage.removeItem("acceso");
            sessionStorage.removeItem("user");
            sessionStorage.removeItem("rol");
            sessionStorage.removeItem("identificador");
            window.location="../SitioWeb/index.html";
        })
        // si ocurre algun error se eliminan los datos guardados en la SessionStorage y se redirecciona al login
        .catch(error => {
            sessionStorage.removeItem("acceso");
            sessionStorage.removeItem("user");
            sessionStorage.removeItem("rol");
            sessionStorage.removeItem("identificador");
            window.location="../SitioWeb/login2-0.html"; 
        });
    },
    // funcion para redireccionar para redirrecionar a la vista de alumno para tareas con su id de user
}
});

Vue.component('pie-pagina',{
    // plantilla para pie de pagina
    template:`
    <footer class="col-12 piePagina p-4 mt-4">
        <p class="text-center h6 text-light">COPYRIGHT © 2019 <a href="index.html" class=text-light>Centro de Desarrollo de Software</a> - TODOS LOS DERECHOS RESERVADOS </p>
    </footer>
    `,
});



Vue.component('user-dropdown', {
    // se reciben parametros para funcionalidad del menu
    props: ['user','rol','id'],
    // plantilla para el menu, el codigo html a utilizar
    template: `
        <li class="nav-item dropdown  mr-5">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{user}}</a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left" aria-labelledby="navbarDropdown">
                <section v-if="rol=='admin' || rol=='estudiante' || rol=='docente'">
                    <a class="dropdown-item estudiantesmodulo" href="../controlNotas/estudiantesmodulo.html" id="notas">Notas</a>
                    <div class="dropdown-divider"></div>
                </section>
                <section v-if="rol=='admin' || rol=='publicador'">
                    <a class="dropdown-item" href="../PublicacionesEventos/crudPublicaciones.html" >Publicaciones y eventos</a> 
                    <div class="dropdown-divider"></div>
                </section>
                <section v-if="rol=='alumno'">
                    <a class="dropdown-item"  @click="tareaAlumno(id)" >Tareas-Alumno</a> 
                    <div class="dropdown-divider"></div>
                </section>
                <section v-if="rol=='admin' || rol=='docente'">
                <a class="dropdown-item" href="../GestionTareas/vistasAlumnos/admin.html" >Tareas-Docente</a> 
                <div class="dropdown-divider"></div>
                </section>
                <a class="dropdown-item" @click="cerrar">Cerrar Sesión</a>          
            </div>          
        </li>
`,
// metodos relacionados al menu
methods:{
    // metodo para cerrar sesion al hacer clic
    cerrar:function() {
        // se obtiene el token de la sesionstorage
        var acc=sessionStorage.getItem("acceso");
        // se desencripta el token 
        var acceso=atob(acc);
        // se realiza la petición ajax para cerrar la sesion y se envia como parametro el token
        axios.post('http://localhost/APICDS/public/api/auth/logout',{
                token:acceso,
            })
            // si se cierra sesion se eliminan los datos guardados en la SessionStorage y se redirecciona al index
        .then(response => {
            sessionStorage.removeItem("acceso");
            sessionStorage.removeItem("user");
            sessionStorage.removeItem("rol");
            sessionStorage.removeItem("identificador");
            window.location="../SitioWeb/index.html";
        })
        // si ocurre algun error se eliminan los datos guardados en la SessionStorage y se redirecciona al login
        .catch(error => {
            sessionStorage.removeItem("acceso");
            sessionStorage.removeItem("user");
            sessionStorage.removeItem("rol");
            sessionStorage.removeItem("identificador");
            window.location="../SitioWeb/login2-0.html"; 
        });
    },
    // funcion para redireccionar para redirrecionar a la vista de alumno para tareas con su id de user
    tareaAlumno:function(id) {
        window.location="../GestionTareas/alumno/vistaAlumno.html?="+id;
    }
}
});




new Vue({
    el: "#refresh",
    data:{
        user:'',
        rol:'',
        id:'',
        menuAcceso:true,
    },
    created(){
        this.refresh();
    },
    methods:{
        refresh:function () {
            var acc=sessionStorage.getItem("acceso");
            if (acc!="" && acc!=null) {
                var acceso = atob(acc);
                axios.post('http://localhost/APICDS/public/api/auth/refresh', {
                    token: acceso,
                })
                .then(response => {
                    sessionStorage.removeItem("acceso");
                    var acceso2 = btoa(response.data.access_token);
                    var user = btoa(response.data.user.name);
                    var rol = btoa(response.data.rol.name);
                    var id = btoa(response.data.user.id);
                    sessionStorage.setItem("acceso", acceso2);
                    sessionStorage.setItem("user", user);
                    sessionStorage.setItem("rol", rol);
                    sessionStorage.setItem("identificador", id);
                    this.user = response.data.user.name;
                    this.rol = response.data.rol.name;
                    this.id = response.data.user.id;
                    this.menuAcceso=true;
                })
                .catch(error => {
                    sessionStorage.removeItem("acceso");
                    sessionStorage.removeItem("user");
                    sessionStorage.removeItem("rol");
                    sessionStorage.removeItem("identificador");
                    // window.location="login.html";
                    
                    window.location="../SitioWeb/index.html";
                    //window.location="login2-0.html"; 
                    this.menuAcceso=false;
                });

            }else{
                this.menuAcceso=false;
            }
        },

    }
});