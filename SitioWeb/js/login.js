Vue.component('menu-basico', {
    // plantilla del menu
    template: `
    <nav class="navbar navbar-expand-lg navbar-light bg-menu sticky-top">
        <!-- imagen de logo  -->
        <a class="navbar-brand" href="../SitioWeb/index.html">
           LOGO
        </a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- links del menu -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item index"> 
                    <a class="nav-link" href="../SitioWeb/index.html">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item quienes_somos">
                    <a class="nav-link" href="../SitioWeb/quienes_somos.html">¿Quiénes somos?</a>
                </li>
                <li class="nav-item main-publicacion">
                    <a class="nav-link" href="../PublicacionesEventos/main-publicacion.html">Publicaciones</a>
                </li>
                <li class="nav-item vistaDeEventos">
                    <a class="nav-link" href="../PublicacionesEventos/vistaDeEventos.html">Eventos</a>
                </li>
                <li class="nav-item contactanos" >
                    <a class="nav-link" href="../SitioWeb/contactanos.html">Contáctanos</a>
                </li>
                <li class="nav-item login2-0">
                    <a class="nav-link btn btn-primary" id="b-sesion" href="../SitioWeb/login2-0.html">Iniciar Sesion</a>
                </li>
            </ul>

        </div>
    </nav>

`,
});




// se crea un aplicacion de vue con el id login
new Vue({
    // se vincula al id login del codigo html
    el: '#login',
    // se definen las variables a utilizar 
    data(){ 
        return {
        user: '',
        pass: '', 
        loading:false,
         }
    },
    // de definen los metodos a utilizar en la aplicacion
    methods: {
        // se crea una funcion que se ejecua 
        login: function() {
            // se cierra o se oculta mensaje de error si se esta mostarndo 
            $(".alert").alert('close');
            // se muestra modal para indicar que se debe esperar 
                $("#modalLogin").modal("show");
                // Se realiza peticion ajax con axios enviando como parametro el email y la contraseña
                axios.post('http://localhost/APICDS/public/api/auth/login',{
                        email:this.user,
                        password: this.pass
                    })
                    // Se ejecuta esta funcion si la peticion se ejecuto sin problemas
                .then(response => {
                    // se recibe el token y se encripta
                    var dat=btoa(response.data.access_token);
                    // se guarda el token encriptado en la Session Storage
                    sessionStorage.setItem("acceso", dat);
                    // se redirecciona a la pagina de inicio
                   window.location="index.html"; 
                })
                // Se ejecuta esta funcion si la peticion NO se ejecuto o di ocurre algun error con las credenciales
                .catch(error => {
                // se muestra mensaje en un alert de bootstrap indicando que contraseña o password son incorrectas
                $("#mensaje").after(`<div class="alert alert-danger alert-dismissible fade show" role="alert">Usuario y/o contraseña incorectas
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button> 
                                    </div`);
                // se indica cerrar el modal despues de 1 segundo de haber finalizado la peticion
                window.setTimeout(function () {
                    $("#modalLogin").modal("hide");
                }, 1000);
                // $("#modalLogin").modal("hide");   
                })
            

        }
      },
});

